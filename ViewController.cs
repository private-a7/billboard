﻿using ARKit;
using CoreGraphics;
using Foundation;
using SceneKit;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace ARKitBillboard
{
    public partial class ViewController : UIViewController
    {

        private UIImagePickerController imagePicker;
        private readonly ARSCNView sceneView;
        private bool isSelecting = false;
        private List<CGImage> images = new List<CGImage>();

        public ViewController(IntPtr handle) : base(handle)
        {
            this.sceneView = new ARSCNView
            {
                AutoenablesDefaultLighting = true,
                Delegate = new SceneViewDelegate()
            };

            this.View.AddSubview(this.sceneView);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.sceneView.Frame = this.View.Frame;

        }

        private void ImagePicker_FinishedPickingMedia(object sender, UIImagePickerMediaPickedEventArgs e)
        {
            var img = e.EditedImage ?? e.OriginalImage;
            images.Add(img.CGImage);
            isSelecting = false;
            refreshSession();
        }

        private void ImagePicker_Canceled(object sender, EventArgs e)
        {
            imagePicker.DismissViewController(true, null);
            isSelecting = false;
            refreshSession();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            //var detectionImages = ARReferenceImage.GetReferenceImagesInGroup("AR Resources", null);
            refreshSession();
        }

        private void refreshSession()
        {
            var detectionImages = new NSSet<ARReferenceImage>(images.Select(x => new ARReferenceImage(x, ImageIO.CGImagePropertyOrientation.Up, 30)).ToArray());

            this.sceneView.Session.Run(new ARWorldTrackingConfiguration
            {
                AutoFocusEnabled = true,
                PlaneDetection = ARPlaneDetection.Horizontal | ARPlaneDetection.Vertical,
                LightEstimationEnabled = true,
                WorldAlignment = ARWorldAlignment.GravityAndHeading,
                DetectionImages = detectionImages,
                MaximumNumberOfTrackedImages = 1

            }, ARSessionRunOptions.ResetTracking | ARSessionRunOptions.RemoveExistingAnchors);
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            if (isSelecting)
            {
                isSelecting = false;
                imagePicker.DismissViewControllerAsync(true);
            }
            else
            {
                isSelecting = true;
                {
                    // Select images.
                    imagePicker = new UIImagePickerController();
                    imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                    imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary);
                    imagePicker.FinishedPickingMedia += ImagePicker_FinishedPickingMedia;
                    imagePicker.FinishedPickingImage += ImagePicker_FinishedPickingImage;
                    imagePicker.Canceled += ImagePicker_Canceled;
                }
                PresentViewController(imagePicker, true,null);
            }
        }

        private void ImagePicker_FinishedPickingImage(object sender, UIImagePickerImagePickedEventArgs e)
        {
            images.Add(e.Image.CGImage);

        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            this.sceneView.Session.Pause();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }
    }

    public class SceneViewDelegate : ARSCNViewDelegate
    {
        public override void DidAddNode(ISCNSceneRenderer renderer, SCNNode node, ARAnchor anchor)
        {
            if (anchor is ARImageAnchor imageAnchor)
            {
                var detectedImage = imageAnchor.ReferenceImage;

                var width = detectedImage.PhysicalSize.Width;
                var length = detectedImage.PhysicalSize.Height;
                var planeNode = new PlaneNode(width, length, new SCNVector3(0, 0, 0), UIColor.Blue);

                float angle = (float)(-Math.PI / 2);
                planeNode.EulerAngles = new SCNVector3(angle, 0, 0);

                node.AddChildNode(planeNode);
            }
        }

        public override void DidRemoveNode(ISCNSceneRenderer renderer, SCNNode node, ARAnchor anchor)
        {
            if (anchor is ARPlaneAnchor planeAnchor)
            {
            }
        }

        public override void DidUpdateNode(ISCNSceneRenderer renderer, SCNNode node, ARAnchor anchor)
        {
            if (anchor is ARPlaneAnchor planeAnchor)
            {
            }
        }
    }

    public class PlaneNode : SCNNode
    {
        public PlaneNode(nfloat width, nfloat length, SCNVector3 position, UIColor colour)
        {
            var rootNode = new SCNNode
            {
                Geometry = CreateGeometry(width, length, colour),
                Position = position
            };

            AddChildNode(rootNode);
        }

        private static SCNGeometry CreateGeometry(nfloat width, nfloat length, UIColor colour)
        {
            var material = new SCNMaterial();
            material.Diffuse.Contents = colour;
            material.DoubleSided = false;

            var geometry = SCNPlane.Create(width, length);
            geometry.Materials = new[] { material };

            return geometry;
        }
    }
}